/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function (){
        var APPLICATION_ID = "CC5BE227-112E-44ED-FF50-84AFEC378400",
        SECRET_KEY = "BEC40234-C5DA-5C92-FF33-54ACF5104D00",
        VERSION = "v1";
        
        Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
        
        var postsCollection = Backendless.Persistence.of(Posts).find();
        
        console.log(postsCollection);
        
        var wrapper = {
            posts:  postsCollection.data
        };
        
        Handlebars.registerHelper('format', function (time){
          return moment(time).format("dddd, MMMM Do YYYY");            
        });
        
        var blogScript = $("#blogs-template").html();
        var blogTemplate = Handlebars.compile(blogScript);
        var blogHTML = blogTemplate(wrapper);
        
        $('.main-container').html(blogHTML);
       
});

function Posts(args){
    args = args || {} ;
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail =  args.authorEmail || "";
    
}